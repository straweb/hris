﻿app.config(function ($routeProvider) {
    $routeProvider

     .when("/ForgotPassword", {

         templateUrl: "ForgotPassword.html"
     })
    .when("/EmployeeDetails", {

        templateUrl: "EmployeeDetails.html"
    })
     .when("/Home", {

         templateUrl: "LandingPage.html"
     })
         .when("/Update", {

             templateUrl: "Angularapp/HtmlPages/EmployeeDetails.html"
         })
         .when("/details", {

             templateUrl: "Angularapp/HtmlPages/EmployeeDataDisplay.html"
         })


 .when("/ConfirmEmail", {

     templateUrl: "Login.html"
 })


 .when("/Logout", {

     templateUrl: "Index.html"
 })
  .when("/ViewProfile", {

      templateUrl: "Angularapp/HtmlPages/EmployeeDataDisplay.html"
  })
   .when("/MyProfile", {
       templateUrl: "Angularapp/HtmlPages/EmployeeDataDisplay.html"
   })
    .when("/EmployeeList", {
        templateUrl: "Angularapp/HtmlPages/EmployeeList.html"
    });

});

